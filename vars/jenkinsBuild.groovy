def call(String gitURL, String gitBranch, String appName, String dockerfilePath ) {
    pipeline {
        agent any
        environment {
            AWS_ACCOUNT_ID="607337111093"
            AWS_REGION="eu-west-1"
            IMAGE_TAG="v1.0.${BUILD_NUMBER}"
        }
        
        stages {
            stage("Checkout Code") {
               steps {
                checkout([
                    $class: 'GitSCM', 
                    branches: [[name: "${gitBranch}" ]], 
                    doGenerateSubmoduleConfigurations: false, 
                    extensions: [], 
                    submoduleCfg: [], 
                    userRemoteConfigs: [[
                        credentialsId: 'gitlab-devops-user', 
                        url: "${gitURL}",
                    ]]
                ]) 
               }
            }

            stage('SonarQube analysis') {
                steps {
                    withSonarQubeEnv("sonarqube") {
                        sh """
                            export PATH=$PATH:/var/lib/jenkins/.dotnet
                            export PATH=$PATH:/var/lib/jenkins/.dotnet/tools
                            dotnet sonarscanner begin /k:${appName} /name:${appName}      
                            dotnet build .
                            dotnet sonarscanner end
                        """           
                    }
                }
            }
            stage("Build") {
                steps {
                    script{
                        app = docker.build("${appName}:${IMAGE_TAG}", "-f ${dockerfilePath} ./")
                    }
                }
            }
            stage("Push") {
                steps {
                    script{
                        docker.withRegistry("https://${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com", "ecr:eu-west-1:aws-credentials") {
                            app.push()
                        }
                    }
                }
            }
            stage("Clean Image") {
                steps {
                    sh """
                        docker rmi ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/${appName}:${IMAGE_TAG}
                    """
                }
            }
            stage("Trigger Deploy Job") {
                steps {
                    build job: "${appName}-DEPLOY", parameters: [[$class: 'StringParameterValue', name: 'imageTag', value: "${IMAGE_TAG}"]],wait: false    
                }     
                
            }
        }
    }
}