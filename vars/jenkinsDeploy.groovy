def call(String appName, String imageTag, String namespace ) {
    pipeline {
        agent any
        environment {
            AWS_ACCOUNT_ID="607337111093"
            AWS_REGION="eu-west-1"
            GIT_URL="http://gitlab.recrigo.com/source-codes/devops/helm.git"
            GIT_BRANCH="master"
        } 
        stages {
            stage("Checkout Code") {
               steps {
                checkout([
                    $class: 'GitSCM', 
                    branches: [[name: "${GIT_BRANCH}" ]], 
                    doGenerateSubmoduleConfigurations: false, 
                    extensions: [], 
                    submoduleCfg: [], 
                    userRemoteConfigs: [[
                        credentialsId: 'gitlab-devops-user', 
                        url: "${GIT_URL}",
                    ]]
                ]) 
               }
            }

            stage("Helm Deploy") {
                steps {
                    sh """
                        helm upgrade --kube-context=arn:aws:eks:eu-west-1:607337111093:cluster/recrigo-nonprod-cluster --install -n ${namespace} -f ./${namespace}/${appName}-values.yaml ${appName} --set deploy.tag=${imageTag} ./
                    """
                }
            }
            stage("Deploy Status") {
                steps {
                    sh """
                        kubectl rollout status deployment ${appName}-deployment -n ${namespace}; 
                    """
                }
            }
        }
    }
}